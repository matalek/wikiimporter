/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import wikilibrary.filtr.Filtr;
import wikilibrary.filtr.FiltrFizyk;
import wikilibrary.filtr.FiltrKażdy;
import wikilibrary.importowanie.Artykuł;
import wikilibrary.importowanie.Encja;
import wikilibrary.importowanie.Importer;
import wikilibrary.importowanie.UnsuccessfulImportException;
import wikilibrary.kategoria.Kategoria;
import wikilibrary.kategoria.KategoriaFilozof;
import wikilibrary.kategoria.KategoriaUrodzenie;
import wikilibrary.klasyfikator.Klasyfikator;
import wikilibrary.klasyfikator.KlasyfikatorSzablonKategoria;
import wikilibrary.przeszukiwanie.Przeszukiwacz;

/**
 *
 * @author aleksander
 */
public class WikiImporter {
    
    private static String odczytajNazwę(String s) {
        return s.replace('_', ' ');
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Importing data...");
        File file = new File(args[0]);
        Importer imp = new Importer(new KlasyfikatorSzablonKategoria(), file);
        Collection<Encja> graf;
        try {
            graf = imp.importuj();
        } catch (UnsuccessfulImportException ex) {
            System.err.println(ex.getMessage());
            System.out.println("Importation has stopped.");
            return;
        }
        System.out.println("Importation has finished.");
        System.out.println("There has been " + graf.size() + " vertices imported.");
                
        Filtr filtrKażdy = new FiltrKażdy();
        Filtr filtrFizyk = new FiltrFizyk();
        Filtr aktualnyFiltr = filtrKażdy;
        Przeszukiwacz p = new Przeszukiwacz(graf, aktualnyFiltr);

        Scanner sc = new Scanner(System.in);
        String s, filtr, nazwa1, nazwa2;

        while(sc.hasNext()) {
            filtr = sc.next();
            nazwa1 = sc.next();
            nazwa2 = sc.next();
            
            nazwa1 = odczytajNazwę(nazwa1);
            nazwa2 = odczytajNazwę(nazwa2);
            switch(filtr) {
                case "all" :
                    aktualnyFiltr = filtrKażdy;
                    break;
                case "physicist" :
                    aktualnyFiltr = filtrFizyk;
                    break;
            }
            p.setFiltr(aktualnyFiltr);
            p.przeszukajIWypisz(nazwa1, nazwa2);
        }

    }
    
}
