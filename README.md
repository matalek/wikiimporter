# Historical Facebook - from Aristotle to Einstein#
## Project created as an assignment for *Object-Oriented Programming* course ##

### Task (shortened) ###
The purpose of this assignment is to create a mechanism which will enable data import to different ontologies. We would concentrate on the ontology containing historical figures. The source of this data should be articles from English Wikipedia. Links inside Wikipedia create a network between articles, which will be the essence of this assignment.

You should create a program, which will classify articles from a prepared subset of Wikipedia articles and will decide whether the given article is about a person. Next, for each article you should get names of people connected with it. We are interested in executing simple queries which will explore the graph between people. We would like, for example, to know how many handshakes are there between Aristotle and Einstein and how the shortest path between them looks. 

### Grade ###
For this assignment I got 9.5 out of 10.0 points.