package wikilibrary.klasyfikator;

import wikilibrary.kryterium.Kryterium;
import wikilibrary.kryterium.KryteriumKategorii;
import wikilibrary.kryterium.KryteriumSzablonu;

/**
 *
 * @author aleksander
 */
public class KlasyfikatorSzablonKategoria extends Klasyfikator{

    public KlasyfikatorSzablonKategoria() {
        kryteria = new Kryterium[]{new KryteriumKategorii(),
            new KryteriumSzablonu()};
    }
    

}
