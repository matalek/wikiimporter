package wikilibrary.klasyfikator;

import wikilibrary.kryterium.Kryterium;
import wikilibrary.importowanie.Artykuł;

/**
 *
 * @author aleksander
 */
public abstract class Klasyfikator {
    protected Kryterium[] kryteria;

    // na podstawie danych kryteriów sprawdza, czy artykuł opisuje osobę
    public boolean czyOsoba(Artykuł art) {
        for (Kryterium k : kryteria)            
            if(k.czyOsoba(art))
                return true;
        return false;
        
    }

}
