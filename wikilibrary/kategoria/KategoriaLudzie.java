package wikilibrary.kategoria;

import java.util.regex.Pattern;

/**
 *
 * @author aleksander
 */
public class KategoriaLudzie extends Kategoria {
    @Override
    public Pattern dajWzór() {
        return Pattern.compile("People .*");
    }
}
