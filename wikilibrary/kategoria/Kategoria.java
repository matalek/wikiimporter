package wikilibrary.kategoria;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import wikilibrary.importowanie.Artykuł;
import wikilibrary.importowanie.Encja;

/**
 *
 * @author aleksander
 */
public abstract class Kategoria {
    
    // daje wzór, który wyznacza daną kategorię
    protected abstract Pattern dajWzór();
    
    // czy wzór danej kategorii odpowiada s
    private boolean czyOdpowiada(String s) {
        Pattern pattern = dajWzór();
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    // czy artykuł należy do danej kategorii
    public boolean czyNależy(Artykuł art) {
        for (String s : art.getKategorie()) {
            if (czyOdpowiada(s))
                return true;
        }
        return false;
    }
    
    // czy encja należy do danej kategorii
    public boolean czyNależy(Encja e) {
        for (String s : e.getKategorie()) {
            if (czyOdpowiada(s))
                return true;
        }
        return false;
    }
    
}
