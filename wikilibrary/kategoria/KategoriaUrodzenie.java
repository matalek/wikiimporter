package wikilibrary.kategoria;

import java.util.regex.Pattern;


/**
 *
 * @author aleksander
 */
public class KategoriaUrodzenie extends Kategoria {

    @Override
    public Pattern dajWzór() {
        return Pattern.compile("(\\d)+ (BC)? births");
    }

    

}
