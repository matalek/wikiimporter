package wikilibrary.kategoria;

import java.util.regex.Pattern;

/**
 *
 * @author aleksander
 */
public class KategoriaŚmierć extends Kategoria {

    @Override
    public Pattern dajWzór() {
        return Pattern.compile("(\\d)+ (BC)? deaths");
    }

}
