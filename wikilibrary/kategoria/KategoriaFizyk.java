package wikilibrary.kategoria;

import java.util.regex.Pattern;

/**
 *
 * @author aleksander
 */
public class KategoriaFizyk extends Kategoria {
    
    @Override
    protected Pattern dajWzór() {
        return Pattern.compile(".*physicist.*");
    }

}
