package wikilibrary.kategoria;

import java.util.regex.Pattern;

/**
 *
 * @author aleksander
 */
public class KategoriaNaukowiec extends Kategoria {
    
    @Override
    protected Pattern dajWzór() {
        return Pattern.compile(".*scientists");
    }

}
