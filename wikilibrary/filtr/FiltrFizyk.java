package wikilibrary.filtr;

import wikilibrary.importowanie.Encja;
import wikilibrary.kategoria.KategoriaFizyk;

/**
 *
 * @author aleksander
 */
public class FiltrFizyk extends Filtr {

    @Override
    public boolean czyMożna(Encja e) {
        return (new KategoriaFizyk()).czyNależy(e);
    }
    

}
