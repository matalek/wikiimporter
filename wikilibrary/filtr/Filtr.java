package wikilibrary.filtr;

import wikilibrary.importowanie.Encja;

/**
 *
 * @author aleksander
 */
public abstract class Filtr {
    
    // czy dana encja spełnia warunek filtry
    public abstract boolean czyMożna(Encja e);

}
