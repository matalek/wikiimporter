package wikilibrary.kryterium;

import wikilibrary.importowanie.Artykuł;
import wikilibrary.kategoria.Kategoria;
import wikilibrary.kategoria.KategoriaFilozof;
import wikilibrary.kategoria.KategoriaFizyk;
import wikilibrary.kategoria.KategoriaLudzie;
import wikilibrary.kategoria.KategoriaNaukowiec;
import wikilibrary.kategoria.KategoriaUrodzenie;
import wikilibrary.kategoria.KategoriaŚmierć;

/**
 *
 * @author aleksander
 */
public class KryteriumKategorii extends Kryterium {
    
    @Override
    public boolean czyOsoba(Artykuł art) {
        Kategoria[] kategorie = {new KategoriaFilozof(), new KategoriaLudzie(),
            new KategoriaNaukowiec(), new KategoriaUrodzenie(), 
            new KategoriaŚmierć(), new KategoriaFizyk()};
        
        for (Kategoria k : kategorie)
            if (k.czyNależy(art))
                return true;
        return false;
    }

}
