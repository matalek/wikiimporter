package wikilibrary.kryterium;

import wikilibrary.importowanie.Artykuł;

/**
 *
 * @author aleksander
 */
public class KryteriumSzablonu extends Kryterium {
    
    @Override
    public boolean czyOsoba(Artykuł art) {
        for (String s : art.getSzablony())
            if (s.startsWith("Persondata"))
                return true;
        return false;
    }

}
