package wikilibrary.kryterium;

import wikilibrary.importowanie.Artykuł;

/**
 *
 * @author aleksander
 */
public abstract class Kryterium {
    
    // czy według danego kryterium artykuł opisuje osobę
    public abstract boolean czyOsoba(Artykuł art);

}
