package wikilibrary.przeszukiwanie;

import wikilibrary.importowanie.Encja;
import wikilibrary.filtr.Filtr;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author aleksander
 */
public class Przeszukiwacz {
    private Collection<Encja> graf;
    private Filtr filtr;

    public Przeszukiwacz(Collection<Encja> graf, Filtr filtr) {
        this.graf = graf;
        this.filtr = filtr;
    }
    
    // przeszukuje graf i zwraca ścieżkę encji od startu do celu
    // zwraca null, jeśli wierzchołek start lub cel nie jest reprezentowany
    // w grafie lub gdy ścieżka nie istnieje
    public Collection<Encja> przeszukaj(String start, String cel) {
        Encja eStart = null, eCel = null;
        ArrayDeque<Encja> q = new ArrayDeque<Encja>();
        // zapamiętuje poprzedników w wyszukiwaniu BFS w celu późniejszego
        // odtworzenia ścieżki:
        HashMap<Encja, Encja> previous = new HashMap<Encja, Encja>();
        
        // znajdowanie startu i celu w grafie encji
        start = start.toLowerCase();
        cel = cel.toLowerCase();
        for (Encja e : graf) {
            if (e.getTytuł().toLowerCase().equals(start))
                eStart = e;
            if (e.getTytuł().toLowerCase().equals(cel))
                eCel = e;
        }
        
        if (eStart == null) {
            System.err.println(
                    "***\nStart vertex is not represented in the graph.\n***");
            return null;
        }
        if (eCel == null) {
            System.err.println(
                    "***\nEnd vertex is not represented in the graph.\n***");
            return null;
        }
        
        ArrayDeque<Encja> ścieżka = new ArrayDeque<Encja>();
        
        // w przypadku gdy start równy celowi zwraca ścieżkę długości 0, 
        // złożoną tylko z tej osoby
        if (eStart == eCel && filtr.czyMożna(eStart)) {
            ścieżka.add(eStart);
            return ścieżka;
        }
        
        if (filtr.czyMożna(eStart)) {
            q.add(eStart);
            previous.put(eStart, eStart);
        }
        
        boolean znaleziono = false;
        while (!q.isEmpty() && !znaleziono) {
            Encja v = q.removeFirst();
            for(Encja w : v.getOdnośnikiZnaneOsoby()) {
                // jeśli wierzchołek spełnia kryterium i nie był dodany do kolejki
                if (filtr.czyMożna(w) && !previous.containsKey(w)) {
                    previous.put(w, v);
                    q.add(w);
                    if (w == eCel) {
                        znaleziono = true;
                        break;
                    }
                }
            }
        }
        
        if (!znaleziono) {
            System.err.println(
                    "***\nA path between these vertices does not exist.\n***");
            return null;
        }
        
        Encja v = eCel;
        while (v != eStart) {
            ścieżka.addFirst(v);
            v = previous.get(v);
        }
        
        ścieżka.addFirst(eStart);
        return ścieżka;
    }
    
    // przeszukuje graf i wypisuje ścieżkę encji od startu do celu
    public void przeszukajIWypisz(String start, String cel) {
        Collection<Encja> ścieżka = przeszukaj(start, cel);
        
        if (ścieżka != null) {
            System.out.println("***");
            System.out.println("Path length: " + (ścieżka.size() - 1));
            for(Encja e : ścieżka)
                System.out.println(e.getTytuł());
            System.out.println("***");
        }
    }

    public void setFiltr(Filtr filtr) {
        this.filtr = filtr;
    }

    public Filtr getFiltr() {
        return filtr;
    }
}
