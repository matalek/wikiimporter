package wikilibrary.importowanie;

import wikilibrary.importowanie.Artykuł;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author aleksander
 */
public class Encja {
    private String tytuł;
    private Collection<String> kategorie;
    private Collection<String> odnośniki;
    private Collection<Encja> odnośnikiZnaneOsoby;
    
    public Encja(Artykuł art) {
        tytuł = art.getTytuł();
        kategorie = art.getKategorie();
        odnośniki = art.getOdnośniki();
        odnośnikiZnaneOsoby = new ArrayList<Encja>();
    }
    
    public void addOdnośnikZnaneOsoby(Encja e) {
        odnośnikiZnaneOsoby.add(e);
    }

    public Collection<String> getOdnośniki() {
        return odnośniki;
    }

    public Collection<Encja> getOdnośnikiZnaneOsoby() {
        return odnośnikiZnaneOsoby;
    }

    public String getTytuł() {
        return tytuł;
    }

    public Collection<String> getKategorie() {
        return kategorie;
    }
    
    
    
    

}
