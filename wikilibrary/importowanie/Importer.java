package wikilibrary.importowanie;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import wikilibrary.klasyfikator.Klasyfikator;

/**
 *
 * @author aleksander
 */
public class Importer {
    private Klasyfikator klasyfikator;
    private File file;
    private HashMap<String, Encja> mapaEncji = new HashMap<String, Encja>();

    public Importer(Klasyfikator klasyfikator, File file) {
        this.klasyfikator = klasyfikator;
        this.file = file;
    }
    
    // sprawdza czy artykuł opisuje osobę i jeśli tak, to ją dodaje 
    private void dodajEwentualnieEncję(Artykuł art) {
        if (klasyfikator.czyOsoba(art)) {
            mapaEncji.put(art.getTytuł(), new Encja(art));
        }
    }
    
    // iportuje encje z pliku
    public Collection<Encja> importuj() throws UnsuccessfulImportException {
        
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            
            mapaEncji.clear();
            
            
            DefaultHandler handler = new DefaultHandler() {
                private String tytuł = "", treść = "";
                private boolean btytuł = false, btreść = false;
                
                public void startElement(String uri, String localName, 
                        String qName, Attributes attributes) throws SAXException {
                    switch(qName) {
                        case "title" :
                            btytuł = true;
                            break;
                        case "text" :
                            btreść = true;
                            break;
                    }
                }
                
                public void endElement(String uri, String localName,
                        String qName) throws SAXException {
                    switch(qName) {
                        case "title" :
                            btytuł = false;
                            break;
                        case "text" :
                            btreść = false;
                            
                            break;
                        case "page" :
                            dodajEwentualnieEncję(new Artykuł(tytuł, treść));
                            treść = "";
                            break;      
                    }
                }
                
                public void characters(char ch[], int start, int length) 
                        throws SAXException {
                    if (btytuł) {
                        tytuł = (new String(ch, start, length));
                    } else if (btreść) {
                        treść += (new String(ch, start, length));
                    }
                }
                
            };
            
            saxParser.parse(file, handler);
            
            // tworzy graf powiązań między zaimportowanymi encjami
            for(String s : mapaEncji.keySet()) {
                Encja e = mapaEncji.get(s);
                for (String odnośnik : e.getOdnośniki())
                    if (mapaEncji.containsKey(odnośnik))
                        e.addOdnośnikZnaneOsoby(mapaEncji.get(odnośnik));
            }
            
        } catch(IOException ex) {
            throw new UnsuccessfulImportException("File not found.");
        } catch (ParserConfigurationException | SAXException ex) {
            throw new UnsuccessfulImportException("XML parser throwed exception.");
        }
        
        return mapaEncji.values();
    }

}
