package wikilibrary.importowanie;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author aleksander
 */

public class Artykuł {
    String treść;
    String tytuł;
    Collection<String> kategorie;
    Collection<String> szablony;
    HashSet<String> odnośniki;
    
    public Artykuł(String tytuł, String treść) {
        this.treść = treść;
        this.tytuł = tytuł;
        kategorie = getFromPattern(Pattern.compile("\\[\\[Category:([^\\]]*)\\]\\]"));
        //usunięcie powtórzeń odnośników
        odnośniki = new HashSet<String>
            (getFromPattern(Pattern.compile("\\[\\[([^\\]|#]+)[^\\]]*\\]\\]")));
        szablony = getFromPattern(Pattern.compile("\\{\\{([^\\}]*)\\}\\}"));
    }
    
    private Collection<String> getFromPattern(Pattern pattern) {
        ArrayList<String> l = new ArrayList<>();
        Matcher matcher = pattern.matcher(treść);
        while (matcher.find()) {
            l.add(matcher.group(1));
        }
        return l;
    }
    
    public Collection<String> getKategorie() {
        return kategorie;
    }
    
    public Collection<String> getSzablony() {
        return szablony;
    }

    public String getTytuł() {
        return tytuł;
    }
    
    public Collection<String> getOdnośniki() {
        return odnośniki;
    }
    

}
