package wikilibrary.importowanie;

/**
 *
 * @author aleksander
 */
public class UnsuccessfulImportException extends Exception{

    UnsuccessfulImportException(String message) {
        super(message);
    }
    

}
